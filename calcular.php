<?php
	
	$cep_o = $_POST["n-cep-o"];
	$cep_d = $_POST["n-cep-d"];
	$serv = $_POST["n-serv"];
	$peso = $_POST["n-peso"];
	$comp = $_POST["n-comp"];
	$larg = $_POST["n-larg"];
	$alt = $_POST["n-alt"];
	$val = $_POST["n-val"];

	$cep_o = str_replace(".", "", str_replace("-", "", $cep_o));
	$cep_d = str_replace(".", "", str_replace("-", "", $cep_d));
	$peso = str_replace(",", ".", str_replace(".", "", $peso));
	$comp = str_replace(",", ".", str_replace(".", "", $comp));
	$larg = str_replace(",", ".", str_replace(".", "", $larg));
	$alt = str_replace(",", ".", str_replace(".", "", $alt));
	$val = str_replace(",", ".", str_replace(".", "", $val));
	
	$frete = calcular($cep_o, $cep_d, $serv, $peso, $comp, $larg, $alt, $val);
	
	function calcular($cep_o, $cep_d, $serv, $peso, $comp, $larg, $alt, $val){
		$api_correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?&sCepOrigem=".$cep_o."&sCepDestino=".$cep_d."&nCdServico=".$serv."&nVlPeso=".$peso."&nVlComprimento=".$comp."&nVlLargura=".$larg."&nVlAltura=".$alt."&nVlValorDeclarado=".$val."&nCdMaoPropia=n&nVlDiametro=0&nCdEmpresa=&sDSenha=&sCdAvisoRecebimento=n&StrRetorno=xml";

		$arq_xml = simplexml_load_file($api_correios);

		if ($arq_xml->cServico->Erro=="0"){
			return $arq_xml;
		} else {
			return $arq_xml->cServico->Erro;
		}

	}

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Calculo de Frete - Resultado</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
</head>
<body>
<div class="container py-5">
	<div class="row mb-5">
		<div class="col-12 text-center">
			<h4>Resultado - Cálculo de Frete</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-12 col-md-6 offset-md-3">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th colspan="2">Cálculo do Frete</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>CEP de Origem:</td>
						<td><?php echo $cep_o; ?></td>
					</tr>
					<tr>
						<td>CEP de Destino:</td>
						<td><?php echo $cep_d; ?></td>
					</tr>
					<tr>
						<td>Tipo de Serviço:</td>
						<td><?php echo ($frete->cServico->Codigo=="41106")?"PAC":"SEDEX"; ?></td>
					</tr>
					<tr>
						<td>Valor:</td>
						<td><?php echo "R$ ".$frete->cServico->Valor; ?></td>
					</tr>
					<tr>
						<td>Prazo de Entrega:</td>
						<td><?php echo ($frete->cServico->PrazoEntrega > "1")? $frete->cServico->PrazoEntrega." dias": $frete->cServico->PrazoEntrega." dia"; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

		<script src="//code.jquery.com/jquery-3.4.1.slim.min.js"></script>
		<script src="//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
		<script src="//stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>
</html>