<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Cálculo de Frete</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

		<style>

		</style>

	</head>
	<body>
		<div class="container py-5">
			<div class="row">
				<div class="col-12 text-center">
					<h4>Cálculo de Frete</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-6 offset-md-3">
					<form action="calcular.php" method="post">
						<div class="form-group">
							<label for="cep-o">CEP Origem</label>
							<input type="text" id="cep-o" name="n-cep-o" maxlength="11" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="cep-d">CEP Destino</label>
							<input type="text" id="cep-d" name="n-cep-d" maxlength="11" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="s-serve">Tipo de Envio</label>
							<select name="n-serv" id="s-serve" class="form-control" required>
								<option value="41106">PAC</option>
								<option value="40010">SEDEX</option>
							</select>
						</div>
						<div class="form-group">
							<label for="id-peso">Peso</label>
							<input type="text" id="id-peso" name="n-peso" onkeyup="mask(this)" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="id-comp">Comprimento</label>
							<input type="text" id="id-comp" name="n-comp" onkeyup="mask(this)" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="id-larg">Largura</label>
							<input type="text" id="id-larg" name="n-larg" onkeyup="mask(this)" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="id-alt">Altura</label>
							<input type="text" id="id-alt" name="n-alt" onkeyup="mask(this)" class="form-control" required>
						</div>
						<div class="form-group">
							<label for="id-val">Valor Declarado</label>
							<input type="text" id="id-val" name="n-val" onkeyup="mask(this)" class="form-control" required>
						</div>
						<div class="form-group text-center">
							<button type="submit" class="btn btn-success btn-block">CALCULAR</button>
						</div>
					</form>
				</div>
			</div>
		</div>

		<script src="//code.jquery.com/jquery-3.4.1.slim.min.js"></script>
		<script src="//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
		<script src="//stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

		<script>
			window.onload = function(){
				var cep_o = document.querySelector('#cep-o');

				cep_o.addEventListener("keyup", () =>{
					var val_cep = cep_o.value.replace(/[^0-9]/g, "").replace(/^([\d]{2})?([\d]{3})?([\d]{3})?/, "$1.$2-$3");
					cep_o.value = val_cep;
				});

				var cep_d = document.querySelector('#cep-d');

				cep_d.addEventListener("keyup", () =>{
					var val_cep = cep_d.value.replace(/[^0-9]/g, "").replace(/^([\d]{2})?([\d]{3})?([\d]{3})?/, "$1.$2-$3");
					cep_d.value = val_cep;
				});
			}

			function mask(v){
				var valor = v.value.replace(/\D/g, "");
				valor = (valor/100).toFixed(2);
				valor = valor.replace(".", ",");
				valor = valor.replace(/(\d)(\d{3})(\d{3}),/g, "$1.$2.$3,");
				valor = valor.replace(/(\d)(\d{3}),/g, "$1.$2,");
				v.value = valor; 
			}
		</script>

	</body>
</html>